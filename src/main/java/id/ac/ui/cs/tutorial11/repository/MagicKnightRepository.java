package id.ac.ui.cs.tutorial11.repository;

import id.ac.ui.cs.tutorial11.model.MagicKnightModel;
import id.ac.ui.cs.tutorial11.model.MagicModel;
import id.ac.ui.cs.tutorial11.enumattr.Position;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MagicKnightRepository extends JpaRepository<MagicKnightModel, String> {
    MagicKnightModel findByName(@Param("name") String name);
    List<MagicKnightModel> findByMagic(@Param("magic_id") MagicModel magic);
    List<MagicKnightModel> findByPosition(@Param("position") Position position);
}